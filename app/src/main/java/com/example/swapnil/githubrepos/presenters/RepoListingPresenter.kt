package com.example.swapnil.githubrepos.presenters

import android.util.Log
import com.example.swapnil.githubrepos.MainActivity
import com.example.swapnil.githubrepos.network.ApiRequestor
import com.example.swapnil.githubrepos.network.models.RepoListingResponse
import com.example.swapnil.githubrepos.views.RepoDetailsFragment
import com.example.swapnil.githubrepos.views.RepoListingFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class RepoListingPresenter : RepoClickedListener {

    lateinit var view: WeakReference<RepoListingFragment>
    lateinit var chosenRepo: RepoListingResponse

    fun getRepoListing(since: Int) {
        ApiRequestor.apiMethods.getRepos(since.toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            view.get()?.updateList(it)
                        },
                        {
                            view.get()?.updateList(ArrayList())
                            Log.e("Swapnil", "Error:${Log.getStackTraceString(it)}")
                        }
                )
    }

    fun showDetailsForRepo(type: RepoDetailsFragment.DetailsType) {
        val activity = view.get()?.context as MainActivity
        activity?.showFragment(chosenRepo.owner, chosenRepo.name, type)
    }

    override fun repoItemClicked(repo: RepoListingResponse) {
        this.chosenRepo = repo
        view.get()?.showOptions()
    }
}

interface RepoClickedListener {
    fun repoItemClicked(repo: RepoListingResponse)
}