package com.example.swapnil.githubrepos.presenters

import android.util.Log
import com.example.swapnil.githubrepos.network.ApiRequestor
import com.example.swapnil.githubrepos.network.models.User
import com.example.swapnil.githubrepos.views.RepoDetailsFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class RepoDetailsPresenter {

    lateinit var view: WeakReference<RepoDetailsFragment>

    fun getRepoContributorListing(owner: User, repoName: String) {
        ApiRequestor.apiMethods.getRepoContributor(owner.login, repoName)
                .map { userList ->
                    MutableList(userList.size, { index -> userList.get(index).login })
                }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            view.get()?.updateList(it)
                        },
                        {
                            view.get()?.updateList(ArrayList())
                            Log.e("Swapnil", "Error:${Log.getStackTraceString(it)}")
                        }
                )
    }

    fun getRepoLanguageListing(owner: User, repoName: String) {
        ApiRequestor.apiMethods.getRepoLanguages(owner.login, repoName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            view.get()?.updateList(ArrayList<String>(it.keySet()))
                        },
                        {
                            view.get()?.updateList(ArrayList())
                            Log.e("Swapnil", "Error:${Log.getStackTraceString(it)}")
                        }
                )
    }

    fun getRepoBranchListing(owner: User, repoName: String) {
        ApiRequestor.apiMethods.getRepoBranches(owner.login, repoName)
                .map { branches ->
                    MutableList(branches.size, { index -> branches.get(index).name })
                }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            view.get()?.updateList(it)
                        },
                        {
                            view.get()?.updateList(ArrayList())
                            Log.e("Swapnil", "Error:${Log.getStackTraceString(it)}")
                        }
                )
    }
}