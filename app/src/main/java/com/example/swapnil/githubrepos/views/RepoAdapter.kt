package com.example.swapnil.githubrepos.views

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.swapnil.githubrepos.R
import com.example.swapnil.githubrepos.network.models.RepoListingResponse
import com.example.swapnil.githubrepos.presenters.RepoClickedListener

class RepoAdapter(private val clickListener: RepoClickedListener) : RecyclerView.Adapter<RepoHolder>() {

    val repos = mutableListOf<RepoListingResponse>()

    override fun onBindViewHolder(holder: RepoHolder, position: Int) {
        holder.bind(repos.get(position), clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.repo_list_item, parent, false)
        return RepoHolder(view)
    }

    override fun getItemCount(): Int {
        return repos.size
    }
}

class RepoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(data: RepoListingResponse, clickListener: RepoClickedListener) {
        this.itemView.findViewById<TextView>(R.id.tvName).text = data.name
        itemView.isClickable = true
        itemView.setOnClickListener {
            clickListener.repoItemClicked(data)
        }
    }
}