package com.example.swapnil.githubrepos.views

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.swapnil.githubrepos.R

class DetailsListingAdapter : RecyclerView.Adapter<DetailsViewHolder>() {

    lateinit var detailsList: List<String>

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        holder.bind(detailsList.get(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.repo_list_item, parent, false)
        return DetailsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (::detailsList.isInitialized) detailsList.size else 0
    }
}

class DetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(data: String) {
        this.itemView.findViewById<TextView>(R.id.tvName).text = data
    }
}