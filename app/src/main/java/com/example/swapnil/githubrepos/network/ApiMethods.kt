package com.example.swapnil.githubrepos.network

import com.example.swapnil.githubrepos.network.models.BranchListingResponse
import com.example.swapnil.githubrepos.network.models.RepoListingResponse
import com.example.swapnil.githubrepos.network.models.User
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiMethods {

    @GET("/repositories")
    @Headers("Accept:application/vnd.github.v3+json")
    fun getRepos(@Query("since") since: String): Observable<List<RepoListingResponse>>

    @GET("/repos/{owner}/{repoName}/contributors")
    @Headers("Accept:application/vnd.github.v3+json")
    fun getRepoContributor(@Path("owner") owner: String, @Path("repoName") repoName: String): Observable<List<User>>

    //GET /repos/:owner/:repo/languages
    @GET("/repos/{owner}/{repoName}/languages")
    @Headers("Accept:application/vnd.github.v3+json")
    fun getRepoLanguages(@Path("owner") owner: String, @Path("repoName") repoName: String): Observable<JsonObject>

    //GET /repos/:owner/:repo/branches
    @GET("/repos/{owner}/{repoName}/branches")
    @Headers("Accept:application/vnd.github.v3+json")
    fun getRepoBranches(@Path("owner") owner: String, @Path("repoName") repoName: String): Observable<List<BranchListingResponse>>
}