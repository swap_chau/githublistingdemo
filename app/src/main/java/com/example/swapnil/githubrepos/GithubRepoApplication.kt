package com.example.swapnil.githubrepos

import android.app.Application
import com.facebook.stetho.Stetho

class GithubRepoApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this);
    }
}