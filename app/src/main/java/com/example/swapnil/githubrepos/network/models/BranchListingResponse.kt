package com.example.swapnil.githubrepos.network.models

import com.google.gson.annotations.SerializedName

data class BranchListingResponse(
        @SerializedName("name") var name: String,
        @SerializedName("commit") var commit: Commit,
        @SerializedName("protected") var protected: Boolean,
        @SerializedName("protection_url") var protectionUrl: String
)