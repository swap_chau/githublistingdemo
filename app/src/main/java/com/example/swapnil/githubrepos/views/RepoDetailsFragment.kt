package com.example.swapnil.githubrepos.views

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.swapnil.githubrepos.R
import com.example.swapnil.githubrepos.network.models.User
import com.example.swapnil.githubrepos.presenters.RepoDetailsPresenter
import java.lang.ref.WeakReference

class RepoDetailsFragment : Fragment() {

    lateinit var detailsListingAdapter: DetailsListingAdapter
    lateinit var owner: User
    lateinit var repoName: String
    lateinit var type: DetailsType
    lateinit var loadingProgress: ProgressBar

    enum class DetailsType(val value: Int) {
        CONTRIBUTORS(0),
        LANGUAGES(1),
        BRANCHES(2)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.repo_details_fragment, container, false)

        val rvRepoList = rootView.findViewById<RecyclerView>(R.id.rvList)
        rvRepoList.layoutManager = LinearLayoutManager(context)
        detailsListingAdapter = DetailsListingAdapter()
        rvRepoList.adapter = detailsListingAdapter

        val presenter = RepoDetailsPresenter()
        presenter.view = WeakReference(this)

        val title:String
        when (type) {
            DetailsType.CONTRIBUTORS -> {presenter.getRepoContributorListing(owner, repoName)
            title="Contributors"}
            DetailsType.BRANCHES -> {presenter.getRepoBranchListing(owner, repoName)
                title="Branches"}
            DetailsType.LANGUAGES -> {presenter.getRepoLanguageListing(owner, repoName)
                title="Languages"}
        }
        loadingProgress = rootView.findViewById<ProgressBar>(R.id.loadingProgress)
        loadingProgress.visibility = View.VISIBLE

        val toolbar = rootView.findViewById(R.id.toolbar) as Toolbar
        toolbar.setTitle(title)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left)
        toolbar.setNavigationOnClickListener(View.OnClickListener() {
            activity?.onBackPressed()
        })
        return rootView
    }

    fun updateList(list: List<String>) {
        detailsListingAdapter.detailsList = list
        detailsListingAdapter.notifyDataSetChanged()
        loadingProgress.visibility = View.GONE
    }
}