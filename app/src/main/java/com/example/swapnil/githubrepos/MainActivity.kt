package com.example.swapnil.githubrepos

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.swapnil.githubrepos.network.models.User
import com.example.swapnil.githubrepos.views.RepoDetailsFragment
import com.example.swapnil.githubrepos.views.RepoListingFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val repoListingFragment = RepoListingFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragmentContainer, repoListingFragment)
        transaction.commit()
    }


    fun showFragment(owner: User, repoName: String, type: RepoDetailsFragment.DetailsType) {
        val repoDetailsFragment = RepoDetailsFragment()
        repoDetailsFragment.owner = owner
        repoDetailsFragment.repoName = repoName
        repoDetailsFragment.type = type
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragmentContainer, repoDetailsFragment)
        transaction.addToBackStack("repoDetailsFragment")
        transaction.commit()
    }
}
