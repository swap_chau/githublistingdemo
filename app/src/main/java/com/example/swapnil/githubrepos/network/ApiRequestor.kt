package com.example.swapnil.githubrepos.network

import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

object ApiRequestor {
    private val SERVER_DEV = "https://api.github.com/"
    val apiMethods: ApiMethods

    init {
        apiMethods = getRetrofit().create(ApiMethods::class.java)
    }

    private fun getRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val spec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                .build()
        val dispatcher = Dispatcher()
        dispatcher.setMaxRequestsPerHost(1)
        dispatcher.setMaxRequests(1)
        val client = OkHttpClient.Builder()
                .dispatcher(dispatcher)
                .addInterceptor { chain ->
                    val request=chain.request()
                    Log.d("Swapnil","Interceptor 1")
                    val response=chain.proceed(request)
                    Log.d("Swapnil","Interceptor 2")
                    response
                }
                .addInterceptor(interceptor).addNetworkInterceptor(StethoInterceptor())
                .connectionSpecs(Collections.singletonList(spec)).build()

        return Retrofit.Builder()
                .baseUrl(SERVER_DEV)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    /*fun request(){

            val requestParams = HashMap<String, Any>()
            requestParams.put("params", parameters)
            requestParams.put("jsonrpc", "2.0")
            requestParams.put("id", 1)
            requestParams.put("method", methodName)

            val obj = JSONObject(requestParams)
            try {
                val jsonResponse = iretrofit.getRepos("0")
                if (jsonResponse.isSuccessful) {
                    val result = JSONObject(jsonResponse.body())
                }
            }catch (e:Exception){

            }
    }*/
}