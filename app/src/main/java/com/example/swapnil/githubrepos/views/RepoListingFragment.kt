package com.example.swapnil.githubrepos.views

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.example.swapnil.githubrepos.R
import com.example.swapnil.githubrepos.network.models.RepoListingResponse
import com.example.swapnil.githubrepos.presenters.RepoListingPresenter
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import java.lang.ref.WeakReference

class RepoListingFragment : Fragment(), View.OnClickListener, SwipyRefreshLayout.OnRefreshListener {

    private lateinit var repoAdapter: RepoAdapter
    private lateinit var presenter: RepoListingPresenter
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
    private lateinit var swipeRefreshLayout: SwipyRefreshLayout
    lateinit var loadingProgress: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.repo_listing_fragment, container, false)

        val rvRepoList = rootView.findViewById<RecyclerView>(R.id.rvList)
        rvRepoList.layoutManager = LinearLayoutManager(context)
        presenter = RepoListingPresenter()
        presenter.view = WeakReference(this)
        repoAdapter = RepoAdapter(presenter)
        rvRepoList.adapter = repoAdapter

        val repoOptionsContainer = rootView.findViewById<ConstraintLayout>(R.id.bottom_sheet)
        repoOptionsContainer.findViewById<TextView>(R.id.tvContributors).setOnClickListener(this)
        repoOptionsContainer.findViewById<TextView>(R.id.tvLanguages).setOnClickListener(this)
        repoOptionsContainer.findViewById<TextView>(R.id.tvBranches).setOnClickListener(this)
        bottomSheetBehavior = BottomSheetBehavior.from(repoOptionsContainer);
        bottomSheetBehavior.isHideable = true
        bottomSheetBehavior.peekHeight = 0
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        swipeRefreshLayout = rootView.findViewById<SwipyRefreshLayout>(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setOnRefreshListener(this)
        loadingProgress = rootView.findViewById<ProgressBar>(R.id.loadingProgress)
        loadingProgress.visibility = View.VISIBLE
        presenter.getRepoListing(0)
        //presenter.getRepoListing(100)

        val toolbar = rootView.findViewById(R.id.toolbar) as Toolbar
        toolbar.setTitle(R.string.app_name)
        return rootView
    }

    fun updateList(repoList: List<RepoListingResponse>) {
        loadingProgress.visibility = View.GONE
        if (repoList.isNotEmpty()) {

            val lastIndex = repoAdapter.repos.lastIndex
            repoAdapter.repos.addAll(repoList)
            repoAdapter.notifyItemRangeInserted(lastIndex + 1, repoList.size)
            swipeRefreshLayout.setRefreshing(false)
        }
    }

    fun showOptions() {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onClick(v: View?) {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        when (v?.id) {
            R.id.tvContributors -> presenter.showDetailsForRepo(RepoDetailsFragment.DetailsType.CONTRIBUTORS)
            R.id.tvLanguages -> presenter.showDetailsForRepo(RepoDetailsFragment.DetailsType.LANGUAGES)
            R.id.tvBranches -> presenter.showDetailsForRepo(RepoDetailsFragment.DetailsType.BRANCHES)
        }
    }

    override fun onRefresh(direction: SwipyRefreshLayoutDirection) {
        val lastRepo = repoAdapter.repos.get(repoAdapter.repos.lastIndex)
        presenter.getRepoListing(lastRepo.id)
    }
}