package com.example.swapnil.githubrepos.network.models

import com.google.gson.annotations.SerializedName

data class Commit(
        @SerializedName("sha") var sha: String,
        @SerializedName("url") var url: String
)